package com.proyectos.proyecto01.servicio;

import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.modelo.Cuenta;

import java.util.List;

public interface  ServicioCliente {

    // ------------- CRUD ----------------------------

    //CREATE
    public void insertarClienteNuevo(Cliente cliente);

    //READ
    public Cliente obtenerCliente(String documento);

    //UPDATE ( solo modificar, no crear)
    public void  guardarCliente(String documento,Cliente cliente);
    //EMPARCHAR
    public  void emparcharCliente(Cliente cliente);


    //DELETE
    public void borrarCliente(String documento);

    //LISTAR
    public  List<Cliente> obtenerClientes();


    void agregarCuentaCliente(String documento, Cuenta cuenta);

    List<Cuenta> obtenerCuentasCliente(String documento);
}
