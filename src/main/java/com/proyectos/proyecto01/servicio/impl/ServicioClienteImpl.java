package com.proyectos.proyecto01.servicio.impl;

import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    public final Map<String,Cliente> clientes = new ConcurrentHashMap<>();

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.clientes.put(cliente.documento,cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        if(!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);

        return this.clientes.get(documento);
    }

    @Override
    public void guardarCliente(String documento,Cliente cliente) {
        if(!this.clientes.containsKey(cliente.documento))
            throw new RuntimeException("No existe el cliente: " + cliente.documento);

        //No se permite reemplazar las cuentas del cliente
        final List<Cuenta> cuentasClientes = obtenerCuentasCliente(cliente.documento);
        cliente.cuentas=cuentasClientes;
        this.clientes.replace(cliente.documento,cliente);
    }

    //DELETE
    @Override
    public void borrarCliente(String documento) {
        if(!this.clientes.containsKey(documento))
            throw new RuntimeException("No existe el cliente: " + documento);
        this.clientes.remove(documento);
    }

    @Override
    public List<Cliente> obtenerClientes() {
        return List.copyOf(this.clientes.values());
    }

    @Override
    public void agregarCuentaCliente(String documento, Cuenta cuenta) {
        final Cliente cliente = this.obtenerCliente(documento);
        cliente.cuentas.add(cuenta);
    }

    @Override
    public List<Cuenta> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.cuentas;
    }

    //Emparchar
    @Override
    public  void emparcharCliente(Cliente parche){
        final Cliente existente = this.clientes.get(parche.documento);
        if(parche.edad!= existente.edad)
            existente.edad=parche.edad;
        if(parche.nombre!= null)
            existente.nombre=parche.nombre;
        if(parche.correo!= null)
            existente.correo=parche.correo;
        if(parche.direccion!= null)
            existente.direccion=parche.direccion;
        if(parche.fechaNacimiento!= null)
            existente.fechaNacimiento=parche.fechaNacimiento;
        if(parche.telefono!= null)
            existente.telefono=parche.telefono;


        this.clientes.replace(existente.documento,existente);
    }


}
