package com.proyectos.proyecto01.Util;

public class Rutas {
    public static final String BASE = "/api/v1";
    public static final String CLIENTES = BASE + "/clientes";
    public static final String CUENTAS = BASE + "/cuentas";
}
