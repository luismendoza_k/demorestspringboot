package com.proyectos.proyecto01.controladores;

import com.proyectos.proyecto01.Util.Rutas;
import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas. CLIENTES+ "/{documento}/cuentas")
public class ControladorCuentasCliente {

    //CRUD - GET * , GET , POST, PUT , PATCH
    @Autowired
    ServicioCliente servicioCliente;

    //POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS ---> Añade una cuenta a las cuentas del cliente
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento , @RequestBody Cuenta cuenta){
        try{
            this.servicioCliente.agregarCuentaCliente(documento,cuenta);}
        catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();

    }

    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuentasCiente(@PathVariable String documento){
        try {
            final List<Cuenta> cuentasCliente= servicioCliente.obtenerCuentasCliente(documento);
            final ResponseEntity respuestaHttpFinal = ResponseEntity.ok().body(cuentasCliente);

            return respuestaHttpFinal;

        }catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
                return ResponseEntity.notFound().build();
        }
    }
}
