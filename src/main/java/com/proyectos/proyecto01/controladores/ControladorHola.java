package com.proyectos.proyecto01.controladores;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hola/v1")
public class ControladorHola {

    String saludo = "Bienvenido a postman!";

    @GetMapping
    public String saludar(){
        return saludo;
    }

    @GetMapping("/saludo")
    public String saludarAlguien(@RequestParam  String persona,@RequestParam String tipo){
        if(tipo.equalsIgnoreCase("welcome")){
            return String.format("Hola %s",persona);
        }
        return String.format("Nos vemos %s",persona);
    }
//    @GetMapping
//    public String saludarHola(){
//        return "Hola!";
//   }
    //PUT http://localhost:9000/hola/v1
    // ¡Chau!
    @PutMapping
    public String modificarSaludo( @RequestParam String saludoNuevo){
        String msg ="";
        msg = String.format(" Cambio ok. De [%s] a [%s]", this.saludo,saludoNuevo);
        this.saludo= saludoNuevo;
        System.out.println(" [ LOG ]  Saludo modificado  : " + saludoNuevo);

        return msg;
    }

    //PUT http://localhost:9000/hola/v1?saludoNuevo='Hola'

//    @PutMapping
//    public String modificarSaludo( @RequestParam String saludoNuevo){
//        String msg ="";
//        msg = String.format(" Cambio ok. De [%s] a [%s]", this.saludo,saludoNuevo);
//        this.saludo= saludoNuevo;
//        System.out.println(" [ LOG ]  Saludo modificado  : " + saludoNuevo);
//
//        return msg;
//    }
}
