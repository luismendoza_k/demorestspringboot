package com.proyectos.proyecto01.controladores;

import com.proyectos.proyecto01.Util.Rutas;
import com.proyectos.proyecto01.modelo.Cliente;
import com.proyectos.proyecto01.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.websocket.server.PathParam;
import java.util.Collections;
import java.util.List;

@RestController
//@RequestMapping("${apirest.url}")
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    //GET http:localhost:9000/api/v1/clientes --> Lista de clientes
    @GetMapping
    public List<Cliente> obtenerClientes(){
        return this.servicioCliente.obtenerClientes();
    }
    // GET http:localhost:9000/api/v1/clientes/{documento} --- > Obtiene un cliente
    @GetMapping("/{documento}")
    public Cliente obtenerCliente(@PathVariable String documento){

        try {
            return this.servicioCliente.obtenerCliente(documento);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //POST http:localhost:9000/api/v1/clientes + DATOS --- >Inserta un cliente
    @PostMapping
    public void agregarCliente(@RequestBody Cliente cliente){
        this.servicioCliente.insertarClienteNuevo(cliente);
    }

    //PUT http:localhost:9000/api/v1/clientes/{documento} + DATOS --> Reemplazar un cliente
    //En caso no coincida el nombre de la ruta con el nombre de la variable Java
    //Reemplaza todo el objeto Json
    @PutMapping("/{documento}")
    public void reemplazarCliente(@PathVariable(name= "documento") String documentoId ,@RequestBody Cliente cliente ){
        try {
            this.servicioCliente.guardarCliente(documentoId, cliente);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{documento}")
    public void empacharCliente(@PathVariable(name= "documento") String documentoId ,@RequestBody Cliente cliente ){
        cliente.documento= documentoId;
        this.servicioCliente.emparcharCliente(cliente);
    }

    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarCliente(@PathVariable(name="documento") String documento){
        try {
            this.servicioCliente.borrarCliente(documento);
        }catch (Exception e){
//            throw new ResponseStatusException(HttpStatus.OK);
        }
    }
}
