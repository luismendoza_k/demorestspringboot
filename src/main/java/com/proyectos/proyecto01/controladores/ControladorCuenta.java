package com.proyectos.proyecto01.controladores;

import com.proyectos.proyecto01.Util.Rutas;
import com.proyectos.proyecto01.modelo.Cuenta;
import com.proyectos.proyecto01.servicio.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas. CUENTAS)
public class ControladorCuenta {

    //CRUD - GET * , GET , POST, PUT , PATCH

    @Autowired
    ServicioCuenta servicioCuenta;

    /**
     *  METODOS SOLO DE CUENTAS
     */

    @GetMapping
    public List<Cuenta> obtenerCuentas(){

        return servicioCuenta.obtenerCuentas();
    }

    @GetMapping("{numero}")
    public Cuenta obtenerCuenta(@PathVariable String numero){
        try {
            return this.servicioCuenta.obtenerCuenta(numero);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta){

        this.servicioCuenta.insertarCuenta(cuenta);
    }

    @PutMapping("/{numero}")
    public void actualizarCuenta(@PathVariable String numero,@RequestBody Cuenta cuenta){
        try{
            this.servicioCuenta.guardarCuenta(numero,cuenta);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{numero}")
    public void borrarCuenta(@PathVariable String numero){
        try {
            this.servicioCuenta.borrarCuenta(numero);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
}
